# GostCrypt Project

GostCrypt project is a fork of TrueCrypt. For more information, see the
[GostCrypt](https://www.gostcrypt.org/) website.

## Installation

All the documentation about the installation process is available on our
[wiki](https://www.gostcrypt.org/wiki/doku.php).


## Pointers

Website : [https://www.gostcrypt.org](https://www.gostcrypt.org)

IRC : #gostcrypt

Twitter : @GostCrypt

PS: This project is developed by students, so be cool ;).
